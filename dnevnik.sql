-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 22, 2019 at 08:35 AM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dnevnik`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `ime` varchar(255) COLLATE utf16_croatian_ci NOT NULL,
  `sifra` varchar(255) COLLATE utf16_croatian_ci NOT NULL,
  `status_admin` varchar(255) COLLATE utf16_croatian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf16 COLLATE=utf16_croatian_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `ime`, `sifra`, `status_admin`) VALUES
(1, 'admin', '123', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `direktor`
--

DROP TABLE IF EXISTS `direktor`;
CREATE TABLE IF NOT EXISTS `direktor` (
  `id` int(255) NOT NULL,
  `ime` varchar(255) COLLATE utf16_croatian_ci NOT NULL,
  `prezime` varchar(255) COLLATE utf16_croatian_ci NOT NULL,
  `status` int(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf16 COLLATE=utf16_croatian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ocene`
--

DROP TABLE IF EXISTS `ocene`;
CREATE TABLE IF NOT EXISTS `ocene` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `predmet` varchar(255) COLLATE utf16_croatian_ci NOT NULL,
  `ocena` int(20) NOT NULL,
  `zakljucna_ocena` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf16 COLLATE=utf16_croatian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `predmet`
--

DROP TABLE IF EXISTS `predmet`;
CREATE TABLE IF NOT EXISTS `predmet` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `ime` varchar(20) COLLATE utf16_croatian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf16 COLLATE=utf16_croatian_ci;

--
-- Dumping data for table `predmet`
--

INSERT INTO `predmet` (`id`, `ime`) VALUES
(1, 'srpski'),
(2, 'matematika'),
(4, 'priroda_i_drustvo'),
(3, 'svet_oko_nas'),
(6, 'fizicko'),
(7, 'muzicko'),
(8, 'veronauka'),
(9, 'likovno'),
(10, 'gradjansko'),
(5, 'istorija');

-- --------------------------------------------------------

--
-- Table structure for table `profesor`
--

DROP TABLE IF EXISTS `profesor`;
CREATE TABLE IF NOT EXISTS `profesor` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `ime` varchar(20) COLLATE utf16_croatian_ci NOT NULL,
  `prezime` varchar(20) COLLATE utf16_croatian_ci NOT NULL,
  `razredni_staresina` varchar(5) COLLATE utf16_croatian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf16 COLLATE=utf16_croatian_ci;

--
-- Dumping data for table `profesor`
--

INSERT INTO `profesor` (`id`, `ime`, `prezime`, `razredni_staresina`) VALUES
(1, 'Dusko', 'Cosic', 'V2'),
(2, 'Pera', 'Peric', 'V3');

-- --------------------------------------------------------

--
-- Table structure for table `raspored`
--

DROP TABLE IF EXISTS `raspored`;
CREATE TABLE IF NOT EXISTS `raspored` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `ponedeljak` varchar(20) COLLATE utf16_croatian_ci NOT NULL,
  `utorak` varchar(20) COLLATE utf16_croatian_ci NOT NULL,
  `sreda` varchar(20) COLLATE utf16_croatian_ci NOT NULL,
  `cetvrtak` varchar(20) COLLATE utf16_croatian_ci NOT NULL,
  `petak` varchar(20) COLLATE utf16_croatian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf16 COLLATE=utf16_croatian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `razred_odeljenje`
--

DROP TABLE IF EXISTS `razred_odeljenje`;
CREATE TABLE IF NOT EXISTS `razred_odeljenje` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `razred` varchar(20) COLLATE utf16_croatian_ci NOT NULL,
  `odeljenje` varchar(20) COLLATE utf16_croatian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf16 COLLATE=utf16_croatian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roditelj`
--

DROP TABLE IF EXISTS `roditelj`;
CREATE TABLE IF NOT EXISTS `roditelj` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `ime` varchar(20) COLLATE utf16_croatian_ci NOT NULL,
  `prezime` varchar(20) COLLATE utf16_croatian_ci NOT NULL,
  `broj` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf16 COLLATE=utf16_croatian_ci;

--
-- Dumping data for table `roditelj`
--

INSERT INTO `roditelj` (`id`, `ime`, `prezime`, `broj`) VALUES
(1, 'Bosko', 'Bubalo', 12312321),
(2, 'Simonida ', 'Pusic', 213123123);

-- --------------------------------------------------------

--
-- Table structure for table `ucenik`
--

DROP TABLE IF EXISTS `ucenik`;
CREATE TABLE IF NOT EXISTS `ucenik` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `ime` varchar(20) COLLATE utf16_croatian_ci NOT NULL,
  `prezime` varchar(20) COLLATE utf16_croatian_ci NOT NULL,
  `predmet` varchar(30) COLLATE utf16_croatian_ci NOT NULL,
  `odeljenje_razred` varchar(5) COLLATE utf16_croatian_ci NOT NULL,
  `ocene` int(5) NOT NULL,
  `jmbg` varchar(20) COLLATE utf16_croatian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf16 COLLATE=utf16_croatian_ci;

--
-- Dumping data for table `ucenik`
--

INSERT INTO `ucenik` (`id`, `ime`, `prezime`, `predmet`, `odeljenje_razred`, `ocene`, `jmbg`) VALUES
(1, 'Mica', 'Petrovic', 'Gradjansko', '5', 333, '43435353535');

-- --------------------------------------------------------

--
-- Table structure for table `ucitelj`
--

DROP TABLE IF EXISTS `ucitelj`;
CREATE TABLE IF NOT EXISTS `ucitelj` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `ime` varchar(20) COLLATE utf16_croatian_ci NOT NULL,
  `prezime` varchar(20) COLLATE utf16_croatian_ci NOT NULL,
  `odeljenje_razred` varchar(30) COLLATE utf16_croatian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf16 COLLATE=utf16_croatian_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
